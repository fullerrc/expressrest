const express = require('express');

const router = express.Router();

/* GET. */
router.get('/', function(req, res) {
    var db = req.db;
    var collection = db.get('items');
    collection.find({},{},function(e,docs){
      res.json(docs);
    });
});

/* POST. */
router.post('/', function(req, res) {
    var db = req.db;
    var collection = db.get('items');
    collection.findOne({ "name": req.body.name }, {}, (e, docs) => {
        if (docs) {
            res.status(409).json({ error: `Item \'${docs.name}\' already exists in database`});
        } else {
            collection.insert(req.body, function(err, result){
                if (err) {
                    res.status(500).json({ error: err });
                } else {
                    collection.findOne({"name": req.body.name},{}, (e,docs) => (
                        res.status(200).json( e ? { error: e } : docs))
                    );
                }
            });
        }
    });
});

/* PUT. */
router.put('/:name', function(req, res) {
    var db = req.db;
    var collection = db.get('items');
    if (req.params.name !== req.body.name) {
        res.status(400).json({ error: 'Malformed request: Queried name does not match body name.' });
    } else {
        collection.update({ name: req.params.name }, req.body, function(err, result){
            if (err) {
                res.status(500).json({ error: err });
            } else {
                collection.findOne({"name": req.body.name},{}, (e,docs) => (
                    res.status(200).json( e ? { error: e } : docs))
                );
            }
        });
    }
});

/* DELETE. */
router.delete('/:name', function(req, res) {
    var db = req.db;
    var collection = db.get('items');
    var itemDeleteName = req.params.name;
    collection.remove({ 'name' : itemDeleteName }, function(err) {
        if (err) {
            res.status(500).json({ error: `${err}` });
        } else {
            res.status(200).json({ message: `${itemDeleteName} deleted.` });
        }
    });
});

module.exports = router;