const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const monk = require('monk');

const config = require('./config');
const itemsRouter = require('./domains/items');

const { nodeHost, nodePort, nodeProtocol } = config.node;
const { mongoHost, mongoPort, mongoDb } = config.mongo;

const db = monk(`${mongoHost}:${mongoPort}/${mongoDb}`);

app.use(bodyParser.json());
app.use(function(req,res,next){
    req.db = db;
    next();
});

app.use('/items', itemsRouter);

app.get('/', (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});

app.listen(nodePort, () => {
 console.log(`Server running on port ${nodeProtocol}://${nodeHost}:${nodePort}/`);
});